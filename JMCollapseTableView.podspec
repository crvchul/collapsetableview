Pod::Spec.new do |s|

  s.name         = "JMCollapseTableView"
  s.version      = "0.2"
  s.summary      = "A UITableView replacement for collapsing UITableviewCells based on sections"

  s.author       = { "Julian Melzig" => "julianmelzig@gmail.com" }

  s.platform     = :ios, '8.0'

  s.homepage     = "http://www.google.de"

  s.source       = { :git => "https://bitbucket.org/crvchul/collapsetableview.git" }

  s.source_files  = 'CollapseTableView/*.{h,m}'

  s.framework  = 'UIKit'

  s.requires_arc = true

end
