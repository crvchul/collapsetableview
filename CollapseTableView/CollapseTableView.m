
//
//  FoldableTableViewController.m
//  grimme
//
//  Created by Julian Melzig on 14.06.13.
//  Copyright (c) 2013 n/a. All rights reserved.
//

#import "CollapseTableView.h"


#define toNSNr(x) ([NSNumber numberWithInteger:x])


#pragma mark Defaults

#define collapse YES



@interface CollapseTableView ()

@property (nonatomic, strong) NSMutableDictionary *currentTableViewValues;

@end

@implementation CollapseTableView

- (id) initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    if (self) {
        [self doInitialSetup];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self doInitialSetup];
    }
    return self;
}


- (void) doInitialSetup
{
    self.unfoldedSections = [NSMutableArray array];
    self.currentTableViewValues = [NSMutableDictionary dictionary];
    self.collapseOtherItems = collapse;
    self.dataSource = self;
    self.delegate = self;
}

- (NSIndexPath *) correctedIndexPath:(NSIndexPath *)indexPath
{
    NSInteger counter = 0;
    
    NSInteger section = 0;
    NSInteger row = 0;
    
    for (int i = 0 ; i < [self.collapseDelegate numberOfSections]; i++) {
        row = 0;
        if ([self.unfoldedSections containsObject:toNSNr(i)]) {
            for (int j = 0; j < [self.collapseDelegate numberOfRowsInSection:i];j++) {
                if (counter == indexPath.row) {
                    return [NSIndexPath indexPathForRow:row inSection:section];
                }
                
                row++;
                counter++;
            }
        }
        
        if (counter == indexPath.row) {
            return [NSIndexPath indexPathForRow:row inSection:section];
        }
        
        counter++;
        section++;
    }
    
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.collapseDelegate numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.unfoldedSections containsObject:toNSNr(section)]) {
        return [self.collapseDelegate numberOfRowsInSection:section]+1;
    }
    else
    {
        return 1;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    if (indexPath.row == 0) {
        return [self.collapseDelegate cellForSection:indexPath.section inTableView:self];
    }
    else
    {
        return [self.collapseDelegate cellForRow:indexPath.row-1 inSection:indexPath.section inTableView:self];
    }

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSIndexPath *modeledIndexPath = indexPath;
    
    if (modeledIndexPath.row == 0) {
        
        if (![self.collapseDelegate sectionShouldStayHighlighted:modeledIndexPath.section]) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        [self.collapseDelegate didSelectSection:modeledIndexPath.section inTableView:self];
        
        // NSDictionary *section = [[self data] objectAtIndex:modeledIndexPath.section];
        
        if ([self.unfoldedSections containsObject:toNSNr(modeledIndexPath.section)]) {
            [self.unfoldedSections removeObject:toNSNr(modeledIndexPath.section)];
            
            NSMutableArray *indexPathesToDelete = [NSMutableArray array];
            NSInteger numberOfRows = [self numberOfRowsInSection:modeledIndexPath.section];
            [self.currentTableViewValues setObject:toNSNr(numberOfRows) forKey:toNSNr(modeledIndexPath.section).stringValue];
            for (int i = 1 ; i < numberOfRows; i++) {
                NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row+i inSection:indexPath.section];
                [indexPathesToDelete addObject:path];
            }
            
            [self.collapseDelegate willHideSection:modeledIndexPath.section inTableView:self];
            
            if (indexPathesToDelete.count > 0) {
                 [self deleteRowsAtIndexPaths:indexPathesToDelete withRowAnimation:UITableViewRowAnimationTop];
            }
           
            

        }
        else
        {            
            NSMutableArray * indexPathesToDelete;
            if (self.collapseOtherItems) {
                indexPathesToDelete = [self indexPathesIfUnfoldedRows];
                
                for (NSNumber *unfoldedSection in self.unfoldedSections) {
                    [self.collapseDelegate willHideSection:unfoldedSection.integerValue inTableView:self];
                }
                
                [self.unfoldedSections removeAllObjects];
            }
            
            
            [self.unfoldedSections addObject:toNSNr(modeledIndexPath.section)];
            NSMutableArray *indexPathesToInsert = [NSMutableArray array];
            
            NSInteger startIndex = 0;
            for (NSIndexPath *indexPathToDelete in indexPathesToDelete) {
                if (indexPathToDelete.row <= indexPath.row) {
                    startIndex++;
                }
            }
            NSInteger numberOfRows = [self.collapseDelegate numberOfRowsInSection:modeledIndexPath.section];
            [self.currentTableViewValues setObject:toNSNr(numberOfRows) forKey:toNSNr(modeledIndexPath.section).stringValue];
            for (int i = 1 ; i <= numberOfRows; i++) {
                NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row+i - startIndex inSection:indexPath.section];
                [indexPathesToInsert addObject:path];
            }
            
            
            [tableView beginUpdates];
            if (indexPathesToInsert.count > 0) {
                [self insertRowsAtIndexPaths:indexPathesToInsert withRowAnimation:UITableViewRowAnimationTop];
            }
            if (indexPathesToDelete.count > 0) {
                [self deleteRowsAtIndexPaths:indexPathesToDelete withRowAnimation:UITableViewRowAnimationTop];
            }
        
            [tableView endUpdates];
            
        }
        
    }
    else
    {
        if (![self.collapseDelegate rowShouldStayHighlighted:modeledIndexPath.row inSection:modeledIndexPath.section]) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        [self.collapseDelegate didSelectRow:modeledIndexPath.row-1 inSection:modeledIndexPath.section inTableView:self];
    }

}

- (NSMutableArray *) indexPathesIfUnfoldedRows
{
    NSMutableArray *indexPathesToDelete = [NSMutableArray array];
    
    for (int i = 0;i< [self.collapseDelegate numberOfSections];i++) {
        if ([self.unfoldedSections containsObject:toNSNr(i)]) {
            for (int j = 1 ; j <= [self.collapseDelegate numberOfRowsInSection:i]; j++) {
                NSIndexPath *path = [NSIndexPath indexPathForRow:j inSection:i];
                [self logIndexPath:path];
                [indexPathesToDelete addObject:path];
            }
            
        }
    }
    return indexPathesToDelete;
}

- (NSInteger) getRowForSection:(NSInteger)section
{
    NSInteger counter = 0;
    for (int i = 0; i < [self.collapseDelegate numberOfSections];i++) {
        if (i == section) {
            break;
        }
        
        if ([self.unfoldedSections containsObject:toNSNr(i)]) {
            counter+=[self.collapseDelegate numberOfRowsInSection:i];
        }
        counter++;
    }
    return counter;
}

- (void) logIndexPath:(NSIndexPath *)path
{
    NSLog(@"IndexPath: Row:%i Section:%i",path.row,path.section);
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *modeledIndexPath = indexPath;
    
    if (modeledIndexPath.row == 0) {
        return [self.collapseDelegate heightForSection:modeledIndexPath.section];
    }
    else
    {
        return [self.collapseDelegate heightForRow:modeledIndexPath.row-1 inSection:modeledIndexPath.section];
    }
}

- (UITableViewCell *) cellForRowAtSection:(NSInteger)section
{
    return [self cellForRow:0 atSection:section];
}
- (UITableViewCell *) cellForRow:(NSInteger)row atSection:(NSInteger)section
{

    return [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
}

- (BOOL) sectionIsExpanded:(NSInteger)section
{
    return [self.unfoldedSections containsObject:toNSNr(section)];
}

- (void) removeSection:(NSInteger)section animated:(BOOL) animated
{

    [self.unfoldedSections removeObject:toNSNr(section)];


    [self beginUpdates];
    [self deleteSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:animated?UITableViewRowAnimationTop:UITableViewRowAnimationNone];
    [self endUpdates];
    
}

- (void) overrideExpandedSections:(NSArray *)sections
{
    [self.unfoldedSections removeAllObjects];
    [self.unfoldedSections addObjectsFromArray:sections];
}


- (void) hideAllSections
{
    [self overrideExpandedSections:@[]];
    [self reloadData];
}

- (void) expandAllSections
{
    NSMutableArray *sections = [NSMutableArray array];
    for (int section = 0; section < self.numberOfSections; section++) {
        [sections addObject:@(section)];
    }

    [self overrideExpandedSections:sections];
    [self reloadData];

}



@end
