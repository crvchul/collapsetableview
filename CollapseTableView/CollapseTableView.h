//
//  FoldableTableViewController.h
//  grimme
//
//  Created by Julian Melzig on 14.06.13.
//  Copyright (c) 2013 n/a. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CollapseTableView;

@protocol CollapseDataSource <NSObject>

- (NSInteger) numberOfSections;
- (NSInteger) numberOfRowsInSection:(NSInteger)section;

- (UITableViewCell *) cellForSection:(NSInteger)section inTableView:(CollapseTableView *)tableView;
- (UITableViewCell *) cellForRow:(NSInteger)row inSection:(NSInteger)section inTableView:(CollapseTableView *)tableView;

- (CGFloat) heightForSection:(NSInteger)section;
- (CGFloat) heightForRow:(NSInteger)row inSection:(NSInteger)section;

- (void) didSelectRow:(NSInteger)row inSection:(NSInteger)section inTableView:(CollapseTableView *)tableView;
- (void) didSelectSection:(NSInteger)section inTableView:(CollapseTableView *)tableView;

- (void) willHideSection:(NSInteger)section inTableView:(CollapseTableView *)tableView;

- (BOOL) sectionShouldStayHighlighted:(NSInteger)section;
- (BOOL) rowShouldStayHighlighted:(NSInteger)row inSection:(NSInteger)section;

@end


@interface CollapseTableView : UITableView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) BOOL collapseOtherItems;
@property (nonatomic, strong) NSMutableArray *unfoldedSections;

@property (nonatomic, strong) id <CollapseDataSource> collapseDelegate;

- (UITableViewCell *) cellForRowAtSection:(NSInteger)section;
- (UITableViewCell *) cellForRow:(NSInteger)row atSection:(NSInteger)section;

- (BOOL) sectionIsExpanded:(NSInteger)section;
- (void) removeSection:(NSInteger)section animated:(BOOL) animateded;
- (void) overrideExpandedSections:(NSArray *)sections;

- (void) hideAllSections;
- (void) expandAllSections;

@end
